

 /* 
  Теоретичні питання
  1.  Логічний оператор - це символ або ключове слово, яке виконує операції порівняння над булевими значеннями (true або false).
 
  2.  AND (&&), OR (||) і NOT (!).
 




  Практичні завдання.
 */
 const ageInput = prompt("Введіть свій вік:");
 const age = parseInt(ageInput);
 
 if (!isNaN(age)) { 
     if (age < 12) {
         alert("Ви  дитина.");
     } else if (age < 18) {
         alert("Ви підліток.");
     } else {
         alert("Ви дорослий.");
     }
 } else {
     alert("Введено не число.");
 }
 
 /*
  2. 
 */
 const monthInput = prompt('Введіть місяць року:');
 const month = monthInput.toLowerCase();
 
 switch (month) {
     case "січень":
     case "березень":
     case "травень":
     case "липень":
     case "серпень":
     case "жовтень":
     case "грудень":
         console.log(`У місяці ${month} 31 день.`);
         break;
     case "квітень":
     case "червень":
     case "вересень":
     case "листопад":
         console.log(`У місяці ${month} 30 днів.`);
         break;
     case "лютий":
         console.log(`У місяці Лютий 28 або 29 днів.`);
         break;
     default:
         console.log("Введено невірний місяць.");
 }

